require("dotenv").config();
const axios = require("axios");

const server = `http://${process.env.AUTH_USER}:${process.env.AUTH_PASS}@localhost:${process.env.LISTEN_PORT}`;
const obsSource = (process.env.TEST_SOURCES || "")
  .split(",")
  .filter((x) => x.replace(/\s/g, "").length > 0);

if (!obsSource.length) {
  console.error("TEST_SOURCES variable not set");
  process.exit(1);
}

function setSourceVisibility(index, visible) {
  console.debug(`setting source: ${obsSource[index]} visible: ${visible}`);
  return axios
    .post(`${server}/SetSceneItemProperties`, {
      item: obsSource[index],
      visible,
    })
    .then((response) => {
      console.debug(response.data);
    })
    .catch((error) => {
      const response = error.response || {};
      console.error(error.code, response.status, response.data);
      process.exit(1);
    });
}

function soloSource(index) {
  for (let i = 0; i < obsSource.length; i++) {
    visibility = index === i;
    setSourceVisibility(i, visibility);
  }
}

function pause(seconds) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, seconds * 1000);
  });
}

async function test() {
  console.debug(
    `This test will connect to OBS Controller Server and switch between the configured sources every 2 seconds: ${obsSource}.`
  );
  while (true) {
    for (let x = 0; x < obsSource.length; x++) {
      soloSource(Number(x));
      await pause(2);
    }
  }
}

test();
