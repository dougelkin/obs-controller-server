#!/usr/bin/env node

require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const OBSWebSocket = require("obs-websocket-js");
const btoa = require("btoa");

const app = express();
const obs = new OBSWebSocket();

function log() {
  console.log(new Date().toISOString(), ...arguments);
}

app.use(bodyParser.json());

app.use((req, res, next) => {
  const user = process.env.AUTH_USER || "";
  const pass = process.env.AUTH_PASS || "";
  const token = `${user}:${pass}`;
  if (req.headers.authorization === `Basic ${btoa(token)}`) {
    next();
  } else {
    res.status(401).json({ error: "unauthorized" });
  }
});

app.get("/", (req, res, next) => {
  res.json({});
});

app.post("/:requestName", async (req, res, next) => {
  const requestId = `${+new Date()}-${Math.random()}`;
  log("INFO", "received-request", requestId, req.params.requestName, req.body);
  try {
    const result = await obs.send(req.params.requestName, req.body || {});
    log("INFO", "returning result", requestId, result);
    res.json(result);
  } catch (e) {
    log("ERROR", requestId, e);
    res.status(400).json({ error: e });
  }
});

const obsConfig = {
  address: process.env.OBS_ADDRESS,
  password: process.env.OBS_PASSWORD,
};

log(
  "INFO",
  "obs-config",
  Object.assign({}, obsConfig, {
    password: obsConfig.password.replace(/./g, "*"),
  })
);

function pause(seconds) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, seconds * 1000);
  });
}

async function OBSconnect() {
  try {
    await obs.connect(obsConfig);
    log("INFO", "obs-websocket connected");
    return;
  } catch (e) {
    log("ERROR", "obs-websocket connection fail reason", e);
  }
}

OBSconnect();
obs.on("ConnectionClosed", async () => {
  log("ERROR", "obs-websocket connection failed. reconnecting...");
  await pause(1);
  await OBSconnect();
});
const httpListen = process.env.LISTEN_PORT || 8008;
app.listen(httpListen);

log("INFO", `obs-controller-server listening on port ${httpListen}`);
