-- for Q-SYS Control Scripting as documented here:
-- https://q-syshelp.qsc.com/Content/Control_Scripting/Using_Lua_in_Q-Sys/01_Using_Lua_in_Q-Sys_Overview.htm

--
-- SETTINGS
--

local OBSControllerIP = '192.168.100.122'
local OBSControllerPort = '8008'
local OBSControllerUser = 'obs-controller-server'
local OBSControllerPass = 'youshouldchangethis'

local OBSsource = {} -- leave this here
-- map input numbers to OBS source names like this:
OBSsource[1] = 'alpha'
OBSsource[2] = 'bravo'
-- OBSsource[3] = 'charlie'
-- OBSsource[4] = 'delta'
-- OBSsource[5] = 'echo'
-- OBSsource[6] = 'foxtrot'

-- this setting is for testing the lua code only
-- mockMode should be false when running this code in Q-SYS environment
local mockMode = true

--
-- FUNCTIONS
--

function setSourceVisibility ( index, visibility )
  local name = OBSsource[index]
  local args = {}
  args['Url'] = 'http://' .. OBSControllerIP .. ':' .. OBSControllerPort .. '/SetSceneItemProperties'
  args['User'] = OBSControllerUser
  args['Password'] = OBSControllerPass
  args['Data'] = '{"item": "' .. name .. '", "visible": ' .. tostring(visibility) .. ' }'
  args['Method'] = 'POST'
  args['EventHandler'] = function (table, code, data, error, headers)
    print('        source ' .. index .. ' response: ', code, data, error)
  end
  headers = {}
  headers['Content-Type'] = 'application/json'
  args['Headers'] = headers
  print('    sending data to OBS controller:', args.Data)
  HttpClient.Upload(args)
end

function soloSource ( index )
  print('    soloSource(' .. index .. ')')
  for i = 1, #OBSsource do
    local visibility = i == index
    setSourceVisibility(i, visibility)
  end
end

function setupEventHandlers ()
  for i = 1, #OBSsource do
    Controls.Inputs[i].EventHandler = function ( changedControl )
      print('Controls.Inputs[' .. i .. '].EventHandler()')
      soloSource(i)
    end
  end
end

function setupMockFunctions ()
  HttpClient = {}
  HttpClient.Upload = function (args)
    args['EventHandler']({}, 200, '{"status": "ok"}', '', {})
  end
  Controls = {}
  Controls.Inputs = {}
  for i = 1, #OBSsource do
    Controls.Inputs[i] = {}
  end
end

function runAmock()
  print('\nmockMode enabled')
  setupMockFunctions()
  setupEventHandlers()
  for i = 1, #OBSsource do
    print('\nmocking input ' .. i .. ':')
    Controls.Inputs[i].EventHandler({})
  end
end

--
-- PROCEDURAL STATEMENTS
--

if (mockMode) then
  runAmock()
else
  setupEventHandlers()
end
