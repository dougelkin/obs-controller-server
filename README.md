# [OBS Controller Server](https://gitlab.com/dougelkin/obs-controller-server)

> a web server interface to the obs-websocket-js library

This is meant to bridge the gap between obs-websocket and environments that cannot run
websocket clients.

## Requirements

- [Node](https://nodejs.org/en/download/) v14+
- [OBS Studio](https://obsproject.com/) v26.1+
- [obs-websocket 4.9.0](https://github.com/Palakis/obs-websocket/releases/tag/4.9.0)

## NPM Commands

npm commands should be executed from within the obs-controller-server directory

### Installing

```cmd
npm install
```

### Running

```cmd
npm start
```

## LICENSE

Copyright © 2021 Douglas Elkin

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
